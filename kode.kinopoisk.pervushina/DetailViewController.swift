//
//  DetailViewController.swift
//  kode.kinopoisk.pervushina
//
//  Created by Eugeniya Pervushina on 5/11/16.
//  Copyright © 2016 air. All rights reserved.
//

import UIKit

let cellsCount = 2

class DetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    var film: Film!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 140
        self.tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return cellsCount
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: DetailsTableViewCell = tableView.dequeueReusableCellWithIdentifier("DetailsTableViewCell") as! DetailsTableViewCell

        if indexPath.row == 0 {
            cell.nameLabel.text = "Name"
            cell.detailLabel.text = film.nameRU
        } else if (indexPath.row == 1) {
            cell.nameLabel.text = "Genre"
            cell.detailLabel.text = film.genre
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100.0
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = UIView(frame:CGRectMake(0, 0, self.view.frame.size.width, 60))
        
        let sessionsButton = UIButton(type: .Custom)
        sessionsButton.setTitle("Available seance", forState: .Normal)
        sessionsButton.addTarget(self, action: #selector(DetailViewController.sessionsButtonTap), forControlEvents: .TouchUpInside)
        sessionsButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        sessionsButton.backgroundColor = UIColor(red: 54/255.0, green: 204/255.0, blue: 223/255.0, alpha: 1.0)
        sessionsButton.layer.cornerRadius = 5.0
        sessionsButton.frame = CGRectMake(footerView.bounds.size.width / 4, 20, footerView.bounds.size.width / 2, 30)
        
        footerView.addSubview(sessionsButton)
        
        return footerView
    }
    
    func sessionsButtonTap() {
        self.performSegueWithIdentifier("toSessions", sender: self)
    }
    
    //MARK: Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "toSessions"){
            let sessionVC: SessionsViewController = segue.destinationViewController as! SessionsViewController
            sessionVC.filmID = self.film.filmID
        }
    }
}
