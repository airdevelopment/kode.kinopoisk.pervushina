//
//  SessionsViewController.swift
//  kode.kinopoisk.pervushina
//
//  Created by Eugeniya Pervushina on 7/11/16.
//  Copyright © 2016 air. All rights reserved.
//

import UIKit
import SwiftyJSON

class SessionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var datePickerView  : UIDatePicker? = UIDatePicker()
    @IBOutlet var tableView: UITableView!
    var filmID: String!
    var sessionList: Array<JSON>!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.sessionList = []
        
        datePickerView!.datePickerMode = UIDatePickerMode.Date
//        sender.inputView = datePickerView
        datePickerView!.addTarget(self, action: #selector(SessionsViewController.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.loadData()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: load data
    func loadData() {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
        indicator.center = CGPoint(x: view.frame.size.width / 2, y: view.frame.size.height / 2)
        self.view.addSubview(indicator)
        indicator.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        let timeStamp = dateFormatter.stringFromDate(NSDate())
        DataManager.sharedInstance.getSeance(self.filmID, cityID: CityManager.sharedInstance.cityId!, date: timeStamp, completionHandler: {(json: JSON?, error: NSError?) -> Void in
            guard error == nil else {
                print(error)
                
                return
            }
            print(json)
            
            if let array = json!["items"].array {
                self.sessionList = array
            }
            
            self.tableView.reloadData()
            
            indicator.stopAnimating()
            indicator.removeFromSuperview()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
        })
    }

    //MARK: TableView
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.sessionList.count
    }
    
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        let dict = self.sessionList[section]
        let sences = dict["seance"].array
        return (sences?.count)!
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell")
        let dict = self.sessionList[indexPath.section]
        let sences = dict["seance"].arrayValue// as! Array<String>
        
        cell?.textLabel?.text = sences[indexPath.row].stringValue
        cell?.detailTextLabel?.text = dict["cinemaName"].stringValue
        
        return cell!
    }
    
    // MARK: - Data picker
    func handleDatePicker(sender: UIDatePicker) {
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
        indicator.center = CGPoint(x: view.frame.size.width / 2, y: view.frame.size.height / 2)
        self.view.addSubview(indicator)
        indicator.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        let timeStamp = dateFormatter.stringFromDate(sender.date)
        DataManager.sharedInstance.getSeance(self.filmID, cityID: CityManager.sharedInstance.cityId!, date: timeStamp, completionHandler: {(json: JSON?, error: NSError?) -> Void in
            guard error == nil else {
                print(error)
                
                return
            }
            print(json)
            
            if let array = json!["items"].array {
                self.sessionList = array
            }
            
            self.tableView.reloadData()
            
            indicator.stopAnimating()
            indicator.removeFromSuperview()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
        })
    }
}
