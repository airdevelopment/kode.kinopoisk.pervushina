//
//  Networking.swift
//  kode.kinopoisk.pervushina
//
//  Created by Eugeniya Pervushina on 5/11/16.
//  Copyright © 2016 air. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SystemConfiguration

typealias NetworkingComplitionHandler = (JSON, NSError?) -> Void

let BASE_URL = "http://api.kinopoisk.cf"
let ECNetworkDomain = "ECNetworkErrorDomain"

enum ECNetworkErrorType : Int {
    
    case noInternet = 1
    case invalidToken = 2
    case accessTokenNotSet = 3
    case invalidRefreshToken = 4
    case invalidVerificationCode = 5
    case unknownNetworkError = 99
}

class Networking: NSObject {

    class var sharedInstance: Networking {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: Networking? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = Networking()
        }
        return Static.instance!
    }
    
    private override init() {
        super.init()
    }
    
    private var manager : Alamofire.Manager = {

        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "http://api.kinopoisk.cf": .PinCertificates(
                certificates: ServerTrustPolicy.certificatesInBundle(),
                validateCertificateChain: true,
                validateHost: true
            )
        ]
        // Create custom manager
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.HTTPAdditionalHeaders = Alamofire.Manager.defaultHTTPHeaders
        
        let man = Alamofire.Manager(
            configuration: NSURLSessionConfiguration.defaultSessionConfiguration()
        )
        return man
    }()

    ///getDatesForSoonFilms
    func getDatesForSoonFilms(completionHandler: NetworkingComplitionHandler) {

        if (networkRechability() == false) {
            let error = NSError.init(domain: ECNetworkDomain, code: ECNetworkErrorType.noInternet.rawValue, userInfo: nil)
            completionHandler(nil, error)
            return
        }
        
        let url = String(BASE_URL + "/getDatesForSoonFilms")
        
        manager.request(.GET, url)
            .responseJSON { response in
                self.finishRequest(response, completionHandler: completionHandler)
        }
    }
    
    func getTodayFilms(date: String, cityID: String, completionHandler: NetworkingComplitionHandler) {
        
        if (networkRechability() == false) {
            let error = NSError.init(domain: ECNetworkDomain, code: ECNetworkErrorType.noInternet.rawValue, userInfo: nil)
            completionHandler(nil, error)
            return
        }
        
        let url = String(BASE_URL + "/getTodayFilms?date=" + date + "&cityID=" + cityID)

        manager.request(.GET, url)
            .responseJSON { response in
                self.finishRequest(response, completionHandler: completionHandler)
        }
    }
    
    func getFilm(filmID: String, completionHandler: NetworkingComplitionHandler) {
        
        if (networkRechability() == false) {
            let error = NSError.init(domain: ECNetworkDomain, code: ECNetworkErrorType.noInternet.rawValue, userInfo: nil)
            completionHandler(nil, error)
            return
        }

        let url = String(BASE_URL + "getFilm?filmID=" + filmID)
        
        manager.request(.GET, url)
            .responseJSON { response in
                self.finishRequest(response, completionHandler: completionHandler)
        }
    }
    
    func getSeance(filmID: String, cityID: String, date: String,completionHandler: NetworkingComplitionHandler) {
        
        if (networkRechability() == false) {
            let error = NSError.init(domain: ECNetworkDomain, code: ECNetworkErrorType.noInternet.rawValue, userInfo: nil)
            completionHandler(nil, error)
            return
        }
        
        let url = String(BASE_URL + "/getSeance?filmID=" + filmID + "&cityID=" + cityID + "&date=" + date)
        
        manager.request(.GET, url)
            .responseJSON { response in
                self.finishRequest(response, completionHandler: completionHandler)
        }
    }
    
    private func networkRechability()-> Bool {
        return isConnectedToNetwork()
    }

    private func finishRequest(response: Alamofire.Response<AnyObject, NSError>, completionHandler: NetworkingComplitionHandler?) {
        switch response.result {
        case .Success:
            
            if let value = response.result.value {
                let json = JSON(value)
                completionHandler!(json, nil)
//                print("JSON: \(json)")
            }
        case .Failure(let error):

            print(error)
            if (completionHandler != nil) {
                completionHandler!(nil, error)
            }
        }
    }
    
    func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}