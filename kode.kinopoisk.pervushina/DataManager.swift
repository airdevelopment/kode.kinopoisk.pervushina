//
//  DataManager.swift
//  kode.kinopoisk.pervushina
//
//  Created by Eugeniya Pervushina on 6/11/16.
//  Copyright © 2016 air. All rights reserved.
//

import UIKit
import SwiftyJSON

let unknownErrorCode = 1
typealias DataManagerComplitionHandler = (JSON?, NSError?) -> Void

@objc(DataManager)
class DataManager: NSObject {

    let errorDomain = "KP.dataManager"
    
    class var sharedInstance: DataManager {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: DataManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = DataManager()
        }
        return Static.instance!
    }
    
    private override init() {
        super.init()
    }
    
    func getDatesForSoonFilms(completionHandler: DataManagerComplitionHandler) {
        Networking.sharedInstance.getDatesForSoonFilms({ (json: JSON, error: NSError?) -> Void in
            
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
//            print(json)
            
        })
    }
    
    func getTodayFilms(date: String, cityID: String, completionHandler: DataManagerComplitionHandler) {
        Networking.sharedInstance.getTodayFilms(date, cityID: cityID, completionHandler:{ (json: JSON, error: NSError?) -> Void in
            
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
//            print(json)
            if let array = json["filmsData"].array {
                for item in array {
//                    print("------------------------------")
//                    print(item)
                    DataBaseManager.sharedInstance.addFilm(item)
                }
            }
            completionHandler(json, error)
        })
    }
    
    func getSeance(filmID: String, cityID: String, date: String, completionHandler: DataManagerComplitionHandler) {
        Networking.sharedInstance.getSeance(filmID, cityID: cityID, date: date, completionHandler:{ (json: JSON, error: NSError?) -> Void in
            
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            print("============================")
            print(json)
            completionHandler(json, nil)
        })
    }
    
    //MARK: dataBase part
    func getFilmsListFromDB() -> [Film] {
        return DataBaseManager.sharedInstance.getAllFilms()
    }
    
    func getOneFilmFromDB(id: String) -> Film {
        return DataBaseManager.sharedInstance.getOneFilmByID(id)
    }
    
    // MARK: converting json    
    func convertStringToDictionary(text: String) -> [String:String]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers) as? [String:String]
                return json
            } catch {
                print("Something went wrong")
            }
        }
        return nil
    }
}



