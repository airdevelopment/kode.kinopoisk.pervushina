//
//  CityManager.swift
//  kode.kinopoisk.pervushina
//
//  Created by Eugeniya Pervushina on 7/11/16.
//  Copyright © 2016 air. All rights reserved.
//

import UIKit

class CityManager: NSObject {
    var cityId: String?
    
    class var sharedInstance: CityManager {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: CityManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = CityManager()
        }
        return Static.instance!
    }
    
    private override init() {
        super.init()
    }
}
