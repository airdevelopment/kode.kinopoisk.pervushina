//
//  Film+CoreDataProperties.swift
//  kode.kinopoisk.pervushina
//
//  Created by Eugeniya Pervushina on 7/11/16.
//  Copyright © 2016 air. All rights reserved.
//

import UIKit

extension Film {
    @NSManaged var filmID: String?
    @NSManaged var webURL: String?
    @NSManaged var nameRU: String?
    @NSManaged var nameEN: String?
    @NSManaged var posterURL: String?
    @NSManaged var year: String?
    @NSManaged var country: String?
    @NSManaged var slogan: String?
    @NSManaged var descriptionIngo: String?
    @NSManaged var rating: NSNumber?
    @NSManaged var genre: String?
}
