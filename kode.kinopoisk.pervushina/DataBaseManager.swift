//
//  DataBaseManager.swift
//  kode.kinopoisk.pervushina
//
//  Created by Eugeniya Pervushina on 5/11/16.
//  Copyright © 2016 air. All rights reserved.
//

import UIKit
import CoreData
import MagicalRecord
import SwiftyJSON

class DataBaseManager: NSObject {
    class var sharedInstance: DataBaseManager {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: DataBaseManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = DataBaseManager()
        }
        return Static.instance!
    }
    
    private override init() {
        super.init()
        MagicalRecord.setupCoreDataStackWithStoreNamed("KinopoiskCoreData")
    }

    
    func addFilm(dict: JSON) {
        let attribute = (dict["id"].string)!
        let array = Film.MR_findByAttribute("filmID", withValue: attribute) as! [Film]
        if array.count > 0 {
            return
        }
        let newFilm = Film.MR_createEntity()
        newFilm!.filmID = (dict["id"].string)!
        newFilm!.nameRU = (dict["nameRU"].string)!
        newFilm!.nameEN = (dict["nameEN"].string)!
        newFilm!.posterURL = (dict["posterURL"].string)!
        newFilm!.year = (dict["year"].string)!
        newFilm!.country = (dict["country"].string)!
        let ratingString = (dict["rating"].string)!.characters.split{$0 == " "}.map(String.init)[0]
        
        newFilm!.rating = NSNumberFormatter().numberFromString(ratingString)!
        newFilm!.genre = (dict["genre"].string)!
        
        self.saveContext()
    }
    
    func getAllFilms() -> [Film] {
        return Film.MR_findAll() as! [Film]
    }
    
    func getOneFilmByID(id: String) -> Film {
        let array = Film.MR_findByAttribute("filmID", withValue: id) as! [Film]
        return array[0]
    }

   // MARK: save
    func saveContext() {
        let defaultContext = NSManagedObjectContext.MR_defaultContext()
        
        defaultContext.MR_saveToPersistentStoreWithCompletion({ (success: Bool, error: NSError?) in
            
            if (success) {
                print("You successfully saved your context.");
            } else if ((error) != nil) {
                print("Error saving context: %@", error!.description);
            }
            
        })
    }
}
