//
//  ViewController.swift
//  kode.kinopoisk.pervushina
//
//  Created by Eugeniya Pervushina on 5/11/16.
//  Copyright © 2016 air. All rights reserved.
//

import UIKit
import SwiftyJSON

enum KPStarsSorting : Int {
    case KPMoreStars = 1
    case KPFewerStars = 2
}

enum KPPickerType : Int {
    case KPCity = 1
    case KPGenres = 2
}

class ListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var cityButton: UIButton!
    @IBOutlet weak var starsButton: UIButton!
    @IBOutlet weak var genreButton: UIButton!
    var films = [Film]()
    var filteredFilms = [Film]()
    
    var pickerView: UIPickerView!
    
    var cityList: NSDictionary!
    var pickerArray: Array<String>!
    var myCityID: String!
    var genreList: Array<String>!
    var pickerType: KPPickerType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cityButton.layer.cornerRadius = 5.0
        
        self.cityList = ["490": "Калининград", "49": "Киев"] //костыльчег
        self.pickerType = KPPickerType.KPCity
        self.pickerArray = cityList.allValues  as! [String]
        
        CityManager.sharedInstance.cityId = "490"
        self.myCityID = CityManager.sharedInstance.cityId
        self.loadData()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: load data
    func loadData() {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
        indicator.center = CGPoint(x: view.frame.size.width / 2, y: view.frame.size.height / 2)
        self.view.addSubview(indicator)
        indicator.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        let timeStamp = dateFormatter.stringFromDate(NSDate())
        let cityID = myCityID
        DataManager.sharedInstance.getTodayFilms(timeStamp, cityID: cityID, completionHandler: {(object: JSON?, error: NSError?) -> Void in
            guard error == nil else {
                print(error)
                
                return
            }
//            print(object)
            self.films = DataManager.sharedInstance.getFilmsListFromDB()
            self.filteredFilms = DataManager.sharedInstance.getFilmsListFromDB()
            self.fillGenres()
            self.tableView.reloadData()
            
            indicator.stopAnimating()
            indicator.removeFromSuperview()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
        })
    }
    
    func fillGenres() {
        var string = "All, "
        for film in self.films {
            string.appendContentsOf(film.genre! + ", ")
        }
        
        let array = string.characters.split{$0 == " "}.map(String.init)
        self.genreList = Array(Set(array))
    }
    
    //MARK: TableView
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.filteredFilms.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: ListTableViewCell = tableView.dequeueReusableCellWithIdentifier("ListTableViewCell") as! ListTableViewCell
        let film = self.filteredFilms[indexPath.row]

        cell.nameLabel?.text = film.nameRU
        cell.starsLabel?.text = film.rating?.stringValue
        cell.genreLabel?.text = film.genre
        
        return cell
    }
    
    //MARK: Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "toDetail"){
            let detailVC:DetailViewController = segue.destinationViewController as! DetailViewController
            let indexPath = self.tableView.indexPathForSelectedRow
            let film = self.filteredFilms[indexPath!.row]
            detailVC.film = film 
        }
    }
    
    //MARK: picker
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerArray.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerArray[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (pickerType == KPPickerType.KPCity) {
            self.cityButton.setTitle(pickerArray[row], forState: UIControlState.Normal)
        } else {
            self.selectionByGenre(self.genreList[row])
            self.genreButton.setTitle(pickerArray[row], forState: UIControlState.Normal)
        }
    }
    
    //MARK: actions
    @IBAction func cityButtonTap(sender: UIButton) {
        let alert = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        alert.modalInPopover = true
        
        pickerView = UIPickerView(frame: CGRectMake(0, 0, self.view.bounds.size.width - 60, 100))
        pickerView?.dataSource = self
        pickerView?.delegate = self
        self.pickerType = KPPickerType.KPCity
        self.pickerArray = cityList.allValues  as! [String]
        alert.view.addSubview(pickerView)
        
        let height:NSLayoutConstraint = NSLayoutConstraint(item: alert.view, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: pickerView.frame.height * 1.6)
        alert.view.addConstraint(height)
        
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
        
        alert.addAction(action)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func starsButtonTap(sender: UIButton) {
        let alert = UIAlertController(title: "Sort by:", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        let actionMore = UIAlertAction(title: "More stars", style: UIAlertActionStyle.Default, handler: { action in self.sortingByStars(KPStarsSorting.KPMoreStars) })
        let actionFewer = UIAlertAction(title: "Fewer stars", style: UIAlertActionStyle.Default, handler: { action in self.sortingByStars(KPStarsSorting.KPFewerStars) })
        alert.addAction(actionMore)
        alert.addAction(actionFewer)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func genreButtonTap(sender: UIButton) {
        let alert = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        alert.modalInPopover = true
        
        pickerView = UIPickerView(frame: CGRectMake(0, 0, self.view.bounds.size.width - 60, 100))
        pickerView?.dataSource = self
        pickerView?.delegate = self
        self.pickerType = KPPickerType.KPGenres
        self.pickerArray = self.genreList
        alert.view.addSubview(pickerView)
        
        let height:NSLayoutConstraint = NSLayoutConstraint(item: alert.view, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: pickerView.frame.height * 1.6)
        alert.view.addConstraint(height)
        
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
        
        alert.addAction(action)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    //MARK: sort methods
    func sortingByStars(method: KPStarsSorting) {
        if (method == .KPMoreStars) {
            var sortingFilms = self.filteredFilms
            sortingFilms.sortInPlace({ $0.rating?.intValue > $1.rating?.intValue })
            self.filteredFilms = sortingFilms
            self.starsButton.setTitle("More stars", forState: UIControlState.Normal)
        } else {
            var sortingFilms = self.filteredFilms
            sortingFilms.sortInPlace({ $0.rating?.intValue < $1.rating?.intValue })
            self.filteredFilms = sortingFilms
            self.starsButton.setTitle("Fewer stars", forState: UIControlState.Normal)
        }
        self.tableView.reloadData()
    }
    
    func selectionByGenre(searchString: String) {
        if (searchString == "All,") {
            self.filteredFilms = self.films
            self.tableView.reloadData()
            return
        }
        let predicate = NSPredicate(format: "genre contains %@", searchString)
        let searchDataSource = self.films.filter { predicate.evaluateWithObject($0) }
        self.filteredFilms = searchDataSource
        self.tableView.reloadData()
    }
}

