//
//  ButtonTableViewCell.swift
//  kode.kinopoisk.pervushina
//
//  Created by Eugeniya Pervushina on 7/11/16.
//  Copyright © 2016 air. All rights reserved.
//

import UIKit

class ButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
